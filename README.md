# LINTEX

TeX Linter (lintex) is a tool to check what is and is not referenced within a tex file. 

![lintex logo](logo.png)

## Usage

	Usage of lintex:
	  -debug
	    	Print debug information.
	  -directory string
	    	Working directory. (default ".")
	  -ignore string
	    	A file containing a list of ignore patterns (default ".gitignore")
	  -main string
	    	Tex file to lint.
	  -match-hidden
	    	Matches on hidden files. i.e files starting with a period '.'.

## Ignore Pattern

Files can be ignored, by default any file starting with a period '.' are ignored unless explicitly stated. If a `.gitignore` file is found in the current directory, that will be used. Otherwise an ignore file can be specified. 

**Note lintex uses .gitignore as a guideline, and is not fully compatible with it.**

- A blank line matches no files, so it can serve as a separator for readability.
- A line starting with # serves as a comment. 
- An asterisk "*" matches file extensions.
- **TODO**: If there is a separator at the end of the pattern then the pattern will only match directories, otherwise the pattern can match both files and directories.

## Example output

### Default

	cat main.tex | lintex 

	Files not referenced:
	 -  README.md
	 -  REFS.bib
	 -  go.mod
	 -  images/not-included.svg
	 -  lintex.png
	 -  main.go
	 -  main.tex
	Files referenced:
	 -  images/gopher.jpg

### With Ignore List

	cat main.tex | lintex -ignore .ignorelintex 
	Files not referenced:
	 -  REFS.bib
	 -  images/not-included.svg
	 -  main.tex
	Files referenced:
	 -  images/gopher.jpg

# Install

You will need golang installed and configured. ``$GOPATH/bin`` will need to be included in your ``$PATH``. 

	git clone https://gitlab.com/PMaynard/lintex.git
	cd lintex
	go install

# Future Work

## Features

- Follow Tex ``\include``
- Correctly match ``\graphicspath``
- Look into ``.cls`` files and find anything they might include.
- Warn when two files with the same name but differing extension are detected. 
- Warn if a table has values (e.g. 80%) but in the body of the text (near a \ref) does not match the table.
- Create archive (i.e zip, tar etc) of used files
- Group output by folder (e.g. tables, figures) 
- Show line number of referrence
- Show which packages are actually required (e.g. \uespacakge{not needed} can be removed) 
- Check links (http/https) return HTTP 200 OK. 

### Modification Options

- Strip comments from .tex
- Remove unneeded files 
- Format tables

## Bugs

- Correctly identify files used which have optional file extensions e.g. .bib, png, tex etc. 
- Check for TeX input before indexing directory.

# License

Copyright 2019 Peter Maynard

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
