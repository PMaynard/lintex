package main

import (
    "flag"
    "fmt"
    "log"
	"path/filepath"
	"os"
	"bufio"
	"io/ioutil"
	"strings"
)

var LISTING 	[]string
var CONTAINS 	[]string
var IGNORE 		[]string
var MATCH_HIDDEN bool
var L Logger

// ==========================================
type Logger struct {
    PrintDebug bool
}

func (m *Logger) Debug(args ...interface{}) {
    if m.PrintDebug {
        m.Println(args...)
        // log.SetFlags(log.Ltime)
    }
}

// func (m *Logger) Set(args ...interface{}) {
//     log.Println(args...)
// }

func (m *Logger) Println(args ...interface{}) {
    log.Println(args...)
}
// ==========================================

func main() {

    L = Logger{}
    log.SetFlags(0)

	// Program flags
	var dir, ignore_file, main_tex string
    var debug bool

    flag.BoolVar(&debug, "debug", false, "Print debug information.")
    flag.StringVar(&dir, "directory", ".", "Working directory.")
    flag.StringVar(&main_tex, "main", "", "Tex file to lint.")
    flag.StringVar(&ignore_file, "ignore", ".gitignore", "A file containing a list of ignore patterns.")
    flag.BoolVar(&MATCH_HIDDEN, "match-hidden", false, "Matches on hidden files. i.e files starting with a period '.'.")
    flag.Parse()

    L.PrintDebug = debug

    // Ignore some things, or don't if no file found.
    dat, err := ioutil.ReadFile(ignore_file)
    if err == nil {
	    for _, v := range strings.Split(string(dat), "\n") {
	    	if !strings.HasPrefix(v, "#") {
	    		IGNORE = append(IGNORE, v)	
	    	}
	    }	
    }    

    L.Debug(IGNORE)
    L.Debug("directory:", dir)

    // Find out all the files 
    if !parse_dirs(dir) {
    	panic("Unable to parse directory: " + dir)
    }

    // Parse Tex
    if !parse_tex(main_tex) {
    	panic("Unable to parse TeX")
    }

    // Display Results
    L.Println("Files not referenced:")
    LISTING = remove_from_slice_by_string(LISTING, main_tex)
    for _, l := range LISTING {
    	L.Println(" - ", l)
    }

    L.Println("Files referenced:")
    if len(CONTAINS) == 0 {
        L.Println(" -  None")
    }
    for _, C := range CONTAINS {
    	L.Println(" - ", C)
    }
}


// Get a recursive list of files 
func parse_dirs(dir string) bool {
	L.Debug(dir)

	err := filepath.Walk(dir, parse_filename)
   	if err != nil {
   		return false
   	}

	return true
}

// find/ignore them...
func parse_filename(p string, info os.FileInfo, err error) error {
    if err != nil {
        return err
    }
    L.Debug(" ", p, info.IsDir())

	if strings.HasPrefix(p, ".") && !MATCH_HIDDEN {
		return nil
	} 

    if info.IsDir() {
    	// TODO: make sure the path added to listing is not absolute. Since most of the time tex uses relative, and it won't pick it up as a match.
    	return nil
    }

    for _, I := range IGNORE {
    	// Ignore if exact match 
    	//  OR
    	//  If starts with a '*'. Check the file extentions.
    	if I == p || (strings.HasPrefix(I, "*") && filepath.Ext(I) == filepath.Ext(p)) {
			L.Debug("[??] Ignored: " + p + " WITH " + I)
    		return nil
    	}

    }

	LISTING = append(LISTING, p)
    return nil
}

// Read file
func parse_tex(file string) bool { 

	var line_num = 0 
    stdin := bufio.NewScanner(os.Stdin)
    
    if file != "" {
        file_reader, err := os.Open(file)
        if err != nil {
            L.Println(err)
            return false
        }
        stdin = bufio.NewScanner(file_reader)
    }

	for stdin.Scan() {
		line_num++
		L.Debug("[SCAN]", stdin.Text())
	
		for i, l := range LISTING {
			// if strings.Contains(stdin.Text(), "\\bibliography") {
			// 	L.Println("BIB")
			// }else{
				// Continue if no match
				if !strings.Contains(stdin.Text(), l) {
					continue
				}

			// }
			
			L.Debug("[LINE]: ", line_num)

			// If parse_match is true. 
			// Add match to CONTAINS and remove from listing.
			CONTAINS = append(CONTAINS, l)
			LISTING = append(LISTING[:i], LISTING[i+1:]...)
		}
	}

	if err := stdin.Err(); err != nil {
        // TODO: Use log?
		fmt.Fprintln(os.Stderr, "error:", err)
		return false 
	}
	return true 
}

func remove_from_slice_by_string(slice []string, val string) []string {
    for i, s := range slice {
        if s == val {
            return append(slice[:i], slice[i+1:]...)
        }
    }
    return slice
}